{ config, lib, pkgs, utils, ... }:

let
  inherit (lib)
    attrNames
    getExe
    mapAttrs'
    mkEnableOption
    mkIf
    mkMerge
    mkOption
    mkPackageOption
    nameValuePair
    optionals
    types;

  inherit (utils) escapeSystemdExecArgs;
  
  cfg = config.services.netbird-client;
in {
  options.services.netbird-client = {
    enable = mkEnableOption "Netbird daemon";
    package = mkPackageOption pkgs "netbird" { };

    logLevel = mkOption {
      type = types.enum [ "debug" "info" "warn" "error" ];
      default = "info";
      description = "Sets the Netbird log level.";
    };

    tunnels = mkOption {
      type = types.attrsOf (types.submodule (import ./tunnel.nix));
      default = { wt0 = { }; };
      description = "Attribute set of Netbird tunnels.  Each tunnel will spawn a separate service.";
    };
  };

  config = mkIf (cfg.tunnels != { }) {
    environment.systemPackages = [ cfg.package ];

    networking.dhcpcd.denyInterfaces = attrNames cfg.tunnels;

    systemd.services = mapAttrs' (name: tunCfg: nameValuePair 
      "netbird-client-${name}"
      {
        description = "A WireGuard-based mesh network that connects your devices into a single private network";
        documentation = [ "https://netbird.io/docs/" ];

        after = [ "network.target" ];
        wantedBy = [ "multi-user.target" ];

        environment = {
          NB_WIREGUARD_PORT = builtins.toString tunCfg.port;
          NB_INTERFACE_NAME = name;
          NB_SKIP_NFTABLES_CHECK = "true"; # we use iptables
        };

        path = with pkgs; [ iptables openresolv ];

        serviceConfig = {
          ExecStart = escapeSystemdExecArgs ([
            (getExe cfg.package)
            "service"
            "run"
            "--config"
            "/var/lib/netbird-client/${name}/config.json"
            "--daemon-addr"
            tunCfg.daemonAddr
            "--log-file"
            "console"
            "--log-level"
            cfg.logLevel
          ]);

          Restart = "always";
          RuntimeDirectory = "netbird-client/${name}";
          StateDirectory = "netbird-client/${name}";
          StateDirectoryMode = "0700";
          WorkingDirectory = "/var/lib/netbird-client/${name}";
        };

        postStart = escapeSystemdExecArgs ([
          (getExe cfg.package)
          "up"
          "--daemon-addr"
          tunCfg.daemonAddr
          "--admin-url"
          tunCfg.adminURL
          "--management-url"
          tunCfg.managementURL
        ] ++ (optionals (tunCfg.setupKey != "") [ "--setup-key" tunCfg.setupKey ]));

        preStop = ''
          ${getExe cfg.package} down --daemon-addr ${tunCfg.daemonAddr}
        '';

        unitConfig = {
          StartLimitInterval = 5;
          StartLimitBurst = 10;
        };

        stopIfChanged = false;
      })
      cfg.tunnels;
  };
}