{
  description = "Netbird VPN Client Service";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";

  outputs = { self, nixpkgs }: let 
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
  in {
    nixosModules.default = import ./modules;
  };
}
